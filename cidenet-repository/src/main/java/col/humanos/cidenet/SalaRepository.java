package col.humanos.cidenet;

import col.humanos.cidenet.model.Sala;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface SalaRepository extends JpaRepository<Sala, BigInteger> {

}
