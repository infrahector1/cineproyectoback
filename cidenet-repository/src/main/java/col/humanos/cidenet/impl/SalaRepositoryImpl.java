package col.humanos.cidenet.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


public class SalaRepositoryImpl {

  public SalaRepositoryImpl() {
    // No es necesario el metodo.
  }

  // Inicializamos el sistema de log
  private static final Logger LOGGER = LoggerFactory.getLogger(SalaRepositoryImpl.class);

  @PersistenceContext
  private EntityManager em;
}