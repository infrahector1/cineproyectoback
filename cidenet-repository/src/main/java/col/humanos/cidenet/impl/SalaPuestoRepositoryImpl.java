package col.humanos.cidenet.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


public class SalaPuestoRepositoryImpl {

  public SalaPuestoRepositoryImpl() {
    // No es necesario el metodo.
  }

  // Inicializamos el sistema de log
  private static final Logger LOGGER = LoggerFactory.getLogger(SalaPuestoRepositoryImpl.class);

  @PersistenceContext
  private EntityManager em;
}