package col.humanos.cidenet.enums;

import java.util.stream.Stream;

public enum TipoSalaEnum {

  DOS_D(1,"2D"), TRES_D(2,"3D"),DYNAMIX(3,"dynamix"),DESCONOCIDO(0,"Desconocido");

  private final Integer id;
  private final String nombre;


  private TipoSalaEnum(Integer id, String nombre) {
    this.id = id;
    this.nombre = nombre;
  }

  public static Stream<TipoSalaEnum> stream() {
    return Stream.of(TipoSalaEnum.values());
  }

  public static TipoSalaEnum getById(Integer id) {
    for (TipoSalaEnum e : values()) {
      if (e.getId() == id) {
        return e;
      }
    }
    return DESCONOCIDO;
  }

  public Integer getId() {
    return id;
  }

  public String getNombre() {
    return nombre;
  }
}
