package col.humanos.cidenet.enums;

import java.util.stream.Stream;

public enum SucursalCiudadEnum {

  SUC_CINECO_MAN(1,"Cine colom manizales"),SUC_CINECO_PER(2,"Cine colom pereira"),DESCONOCIDO(0,"Desconocido");

  private final Integer id;
  private final String nombre;


  private SucursalCiudadEnum(Integer id, String nombre) {
    this.id = id;
    this.nombre = nombre;
  }

  public static Stream<SucursalCiudadEnum> stream() {
    return Stream.of(SucursalCiudadEnum.values());
  }

  public static SucursalCiudadEnum getById(Integer id) {
    for (SucursalCiudadEnum e : values()) {
      if (e.getId() == id ) {
        return e;
      }
    }
    return DESCONOCIDO;
  }

  public Integer getId() {
    return id;
  }

  public String getNombre() {
    return nombre;
  }
}

