package col.humanos.cidenet.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigInteger;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SalaDTO implements Serializable {

  private BigInteger id;

  private BigInteger codSucursalCiudad;

  private String nomSucursal;

  private Integer numeroFila;

  private Integer nroMaximoSillaFila;

  private BigInteger codTipoSala;
  private String nomTipoSala;

  private String salaSilla;
}
