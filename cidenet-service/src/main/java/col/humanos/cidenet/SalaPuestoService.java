package col.humanos.cidenet;

import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;

@Transactional(readOnly = true)
public interface SalaPuestoService {

  @Transactional
  void crearSalaPuestos(String salaPuesto, BigInteger idSala );

}
