package col.humanos.cidenet.serviceimpl;

import col.humanos.cidenet.SalaPuestoRepository;
import col.humanos.cidenet.SalaPuestoService;
import col.humanos.cidenet.enums.EstadoSalaSillaEnum;
import col.humanos.cidenet.model.SalaPuesto;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service
public class SalaPuestoServiceImpl implements SalaPuestoService {

  /** The Constant LOGGER. */
  private static final Logger LOGGER = LoggerFactory.getLogger(SalaPuestoServiceImpl.class);

  private SalaPuestoRepository salaPuestoRepository;

  @Autowired
  public SalaPuestoServiceImpl(SalaPuestoRepository salaPuestoRepository) {
    this.salaPuestoRepository=salaPuestoRepository;
  }


  @Override
  public void crearSalaPuestos(String salaPuesto, BigInteger idSala ){
    String[] salaPuestos=StringUtils.split(salaPuesto,"|");

    for(int i=0; i < salaPuestos.length;i++){
      SalaPuesto salaPuestoObj=new SalaPuesto();
      salaPuestoObj.setCodSala(idSala);
      if(StringUtils.contains(salaPuestos[i],"\r\n")) {
        String[] saltoFila=StringUtils.split(salaPuestos[i],"\r\n");
        if(saltoFila.length > 1) {
          SalaPuesto salaPuestoAux=new SalaPuesto();
          salaPuestoAux.setCodSala(idSala);
          salaPuestoAux.setEstado(EstadoSalaSillaEnum.LIBRE.getId());
          salaPuestoAux.setNumeroFilaConsecutivo(saltoFila[1].trim());
          salaPuestoRepository.save(salaPuestoAux);
        }
        salaPuestoObj.setNumeroFilaConsecutivo(saltoFila[0].trim());
      }else{
        salaPuestoObj.setNumeroFilaConsecutivo(salaPuestos[i].trim());
      }
      salaPuestoObj.setEstado(EstadoSalaSillaEnum.LIBRE.getId());
      salaPuestoRepository.save(salaPuestoObj);
    }
  }
}
