package col.humanos.cidenet.serviceImplTest;


import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import col.humanos.cidenet.SalaPuestoService;
import col.humanos.cidenet.SalaRepository;
import col.humanos.cidenet.SalaService;
import col.humanos.cidenet.dto.SalaDTO;
import col.humanos.cidenet.model.Sala;
import col.humanos.cidenet.serviceimpl.SalaServiceImpl;
import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class SalaServiceImplTest {

  @Autowired
  private SalaService salaService;

  @Mock
  private SalaRepository salaRepository;

  @Mock
  private SalaPuestoService salaPuestoService;

  @Before
  public void setup(){
    salaService=new SalaServiceImpl(salaRepository,salaPuestoService);
  }

  private List<Sala> getSalas(){
    List<Sala> salaList= new ArrayList<>();
    Sala sala= new Sala();
    sala.setCodSucursalCiudad(BigInteger.valueOf(1));
    sala.setCodTipoSala(BigInteger.valueOf(1));
    salaList.add(sala);
    return salaList;
  }

  private SalaDTO getSalaDTO(){
    SalaDTO salaDTO=new SalaDTO();
    return salaDTO;
  }

  private Sala getSala(){
    Sala sala= new Sala();
    sala.setId(BigInteger.valueOf(1));
    return sala;
  }


  @Test
  public void busquedaAvanzada(){
    when(salaRepository.findAll()).thenReturn(null);
    salaService.busquedaAvanzada();
  }

  @Test
  public void busquedaAvanzada1(){

    when(salaRepository.findAll()).thenReturn(getSalas());
    salaService.busquedaAvanzada();
  }

  @Test
  public void crearSala(){
    when(salaRepository.save(getSala())).thenReturn(getSala());
    salaService.crearSala(getSalaDTO());
  }
}
